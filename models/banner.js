const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const bannerSchema = new Schema({
    name: {
        type: String,
        required: false
    },
    imageUrl: {
        type: String,
        required: true
    },
    description: {
        type: String,
        required: false
    },
});
module.exports = mongoose.model('Banner', bannerSchema);
