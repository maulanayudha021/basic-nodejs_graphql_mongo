const { buildSchema } = require('graphql');

const user = require('./schema/userSchema');
const product = require('./schema/productSchema');
const category = require('./schema/categorySchema');
const banner = require('./schema/bannerSchema');
const root = require('./schema/rootSchema');

const result = user+product+category+banner+root;
module.exports = buildSchema(result);