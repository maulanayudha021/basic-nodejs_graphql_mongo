module.exports = `
    type Banner {
        id: ID!
        name: String
        imageUrl: String!
        description: String
    }
    type Banners {
        result: [Banner!]!
    }

    input BannerData {
        name: String
        imageUrl: String!
        description: String
    }
    input BannerDataWithId {
        id: ID!
        name: String
        imageUrl: String!
        description: String
    }
`;