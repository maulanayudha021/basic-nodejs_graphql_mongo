const Banner = require('../../models/banner');

module.exports = {
    getBanners: async function({}, req) {
        const banners = await Banner.find();
        return {
            result: banners.map(b => {
                return {
                    ...b._doc,
                    id: b._id.toString()
                };
            })
        };
    },

    getBanner: async function(input, req) {
        const id = input.id;

        // const user = await User.findOne({ name: input.name }); // get single data by name
        const banner = await Banner.findById(id).exec();
        if (!banner) {
            const error = new Error('Banner is not found!');
            throw error;
        }

        return { 
            ...banner._doc,
            id: banner._id.toString()
        };
    },

    createBanner: async function({ input }, req) {
        // const existingBanner = await Banner.findOne({ name: input.name });
        // if (existingBanner) {
        //     const error = new Error('User exists already!');
        //     throw error;
        // }

        const banner = new Banner(input);        
        const createdBanner = await banner.save();
        return { ...createdBanner._doc, id: createdBanner._id.toString() };
    },

    updateBanner: async function({ input }, req) {
        const id = input.id;

        let selectedBanner = await Banner.findById(id);
        if (!selectedBanner) {
            const error = new Error('User is not found!');
            throw error;
        }

        selectedBanner.name = input.name;
        selectedBanner.imageUrl = input.imageUrl;
        selectedBanner.description = input.description;
        
        if (!input.imageUrl) {
            const error = new Error('Image URL is required!');
            throw error;
        }

        const updatedBanner = await selectedBanner.save();
        const banner = await Banner.findById(id).exec();
        return { 
            ...updatedBanner._doc,
            id: banner._id.toString()
        };
    },

    deleteBanner: async function(input, req) {
        const id = input.id;
        
        const banner = await Banner.findById(id).exec();
        if (!banner) {
            const error = new Error('Banner is not found!');
            throw error;
        }
        
        await Banner.findByIdAndRemove(id);
        return true;
    },
};